<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Use plain text email</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen" />
  </head>
  <body>
    <h1>Use plain text email</h1>
    <p>
      There are two main types of emails on the internet: plain text and HTML.
      Many people, particularly in technical communities, strongly prefer or
      even require the use of plain text email from participants. However, your
      mail client may not have it set up by default. We'll help you get it
      configured, and introduce you to the norms and conventions of plain text
      email.
    </p>
    <h2>Table of Contents</h2>
    <ol>
      <li>
        <a href="#recommended-clients">
          Recommended email clients for plain text users
        </a>
      </li>
      <li>
        <a href="#other-clients">
          How to set up plain text with other email clients
        </a>
      </li>
      <li>
        <a href="#etiquette">Etiquette recommendations for plain text emails</a>
      </li>
      <li>
        <a href="#why-plaintext">Why is plain text better than HTML?</a>
      </li>
      <li>
        <a href="#implementation-recommendations">
          Recommendations for software which sends emails
        </a>
      </li>
      <li>
        <a href="#contribute">How to improve these recommendations</a>
      </li>
    </ol>
    <p class="muted">
      This guide is brought to you by <a
        href="https://sourcehut.org"
      >sourcehut</a>, the "hacker's forge" for software development, a platform
      which utilizes plain text emails for many features. 100% free software and
      no JavaScript required -
      <a href="https://meta.sr.ht/register">try it today</a>!
    </p>
    <img
      src="/assets/certified.svg"
      alt="Plain text certified stamp"
      style="float: right"
      width="200" height="200" />
    <h2 id="recommended-clients"><a href="#recommended-clients">
      Recommended email clients for plain text users
    </a></h2>
    <p>
      The following email clients are known to handle plain text especially well:
    </p>
    <ul>
      <li>
        <a rel="noopener" href="https://aerc-mail.org">aerc</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://repo.or.cz/alpine.git">alpine</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://github.com/wangp/bower">bower</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://www.claws-mail.org/">Claws Mail</a> (GUI)
      </li>
      <li>
        <a rel="noopener" href="https://www.cypht.org/">Cypht</a> (Webmail)
      </li>
      <li>
        <a rel="noopener" href="https://gnus.org/">Gnus</a> (emacs)
      </li>
      <li>
        <a rel="noopener" href="https://kde.org/applications/office/org.kde.kmail2">KMail</a> (GUI)
      </li>
      <li>
        <a rel="noopener" href="https://freron.com/">MailMate</a> (GUI)
      </li>
      <li>
        <a rel="noopener" href="https://github.com/leahneukirchen/mblaze">mblaze</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://meli.delivery/">meli</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="http://marmaro.de/prog/mmh/">mmh</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://djcbsoftware.nl/code/mu/mu4e.html">mu4e</a> (emacs)
      </li>
      <li>
        <a rel="noopener" href="http://mutt.org">Mutt</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://github.com/aligrudi/neatmail">neatmail</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://neomutt.org/">NeoMutt</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://rand-mh.sourceforge.io/">nmh</a> (TUI)
      </li>
      <li>
        <a rel="noopener" href="https://notmuchmail.org/notmuch-emacs/">Notmuch</a> (emacs)
      </li>
      <li>
        <a rel="noopener" href="#roundcube">Roundcube</a> (Webmail)
      </li>
      <li>
        <a rel="noopener" href="https://www.squirrelmail.org/">SquirrelMail</a> (Webmail)
      </li>
      <li>
        <a rel="noopener" href="https://github.com/wanderlust/wanderlust">Wanderlust</a> (emacs)
      </li>
    </ul>
    <p>
      These clients all compose plain text emails by default, with correct
      quoting and text wrapping settings, requiring no additional configuration
      to use correctly.
      <a href="#contribute">Want to add your own mail client to this list</a>?
    </p>
    <h2 id="other-clients"><a href="#other-clients">
      How to set up plain text with other email clients
    </a></h2>
    <p>
      If your email provider's webmail doesn't have good plain text support,
      please consider writing to <code>&lt;postmaster@yourdomain.com&gt;</code>
      with a complaint, and using one of the <a
      href="#recommended-clients">recommended clients</a> over IMAP &amp; SMTP
      instead.
    </p>
    <p>Jump to:</p>
    <ul>
      <li><a href="#afterlogic">Afterlogic</a></li>
      <li><a href="#alot">alot</a></li>
      <li><a href="#macos">Apple Mail</a></li>
      <li><a href="#cypht">Cypht</a></li>
      <li><a href="#disroot">Disroot</a></li>
      <li><a href="#evolution">Evolution</a></li>
      <li><a href="#fairemail">FairEmail</a></li>
      <li><a href="#fastmail">Fastmail</a></li>
      <li><a href="#geary">Geary</a></li>
      <li><a href="#gmail-web">Gmail</a></li>
      <li><a href="#gyazmail">Gyazmail</a></li>
      <li><a href="#k-9">K-9 Mail</a></li>
      <li><a href="#mailbox-org">mailbox.org</a></li>
      <li><a href="#mailspring">Mailspring</a></li>
      <li><a href="#outlook-desktop">Microsoft Outlook (Desktop)</a></li>
      <li><a href="#outlook-web">Microsoft Outlook (Web)</a></li>
      <li><a href="#nine">Nine</a></li>
      <li><a href="#posteo">Posteo</a></li>
      <li><a href="#protonmail">Proton Mail</a></li>
      <li><a href="#rainloop">Rainloop</a></li>
      <li><a href="#roundcube">Roundcube</a></li>
      <li><a href="#runbox7">Runbox 7</a></li>
      <li><a href="#smail">Smail</a></li>
      <li><a href="#sogo">Sogo</a></li>
      <li><a href="#sylpheed">Sylpheed</a></li>
      <li><a href="#thunderbird">Thunderbird</a></li>
      <li><a href="#thunderbird-android">Thunderbird for Android</a></li>
      <li><a href="#tutanota">Tuta</a></li>
      <li><a href="#windows-mail">Windows Mail</a></li>
    </ul>
    <p>
      After configuring your client, be sure to review our
      <a href="#etiquette">etiquette recommendations</a>.
    </p>
    <h3 id="afterlogic">Afterlogic</h3>
    <div class="breakdown">
      <span class="red">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <p>
      Afterlogic does not support plain text email. Please
      <a
        href="https://s.afterlogic.com/forum/forum_posts.asp?TID=2438"
      >ask them for it</a> and use a different mail client.
    </p>
    <h3 id="alot">alot</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <p>alot uses plain text email by default.</p>
    <h3 id="macos">Apple Mail</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <ol>
      <li>Select Mail → Preferences from the menu</li>
      <li>Visit the "Composing" category</li>
      <li>Change the "Message Format"  (or "Format") option to "Plain Text"</li>
    </ol>
    <h3 id="cypht">Cypht</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <h3 id="disroot">Disroot</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>Visit Settings → Preferences from the menu </li>
      <li>Select Composing messages</li>
      <li>Set Compose HTML messages to "never"</li>
    </ol>
    <h3 id="evolution">Evolution</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <p>Plain text is the default. To enable bottom-posting:</p>
    <ol>
      <li>Select Edit → Preferences from the menu</li>
      <li>Select Composer Preferences → General</li>
      <li>Enable "Start typing at the bottom"</li>
    </ol>
    <p>You can also prefer to show the plain text email by default when viewing
    multipart messages:</p>
    <ol>
      <li>Select Edit → Preferences from the menu</li>
      <li>Select Mail Preferences → HTML Messages</li>
      <li>Select your preference from the "HTML Mode" options</li>
    </ol>
    <h3 id="fairemail">FairEmail</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>Visit Settings → Send</li>
      <li>Scroll to the Message section</li>
      <li>Toggle "Write below the sender's text"</li>
      <li>Scroll to the Advanced section</li>
      <li>Toggle "Send plain text only by default"</li>
      <li>Toggle "'format flowed' for plain text"</li>
      <li>Toggle "Follow Usenet signature convention"</li>
    </ol>
    <p>When replying, use "Reply to all" instead of "Reply to list".</p>
    <h3 id="fastmail">Fastmail</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>Visit Settings → Signatures & Compose → Compose options</li>
      <li>In the "Compose" section, change "Compose Format" to "plain text"</li>
      <li>In the same section, disable "When replying, use the same format as the original message"</li>
      <li>In the "When replying" section, change "Include the original message" to "As quoted text"</li>
    </ol>
    <p>To wrap text, press Ctrl+M in the composition view.</p>
    <h3 id="geary">Geary</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>Compose a new message</li>
      <li>On the bottom right, click the three-dotted icon</li>
      <li>Select the plain text option</li>
    </ol>
    <p>Geary will remember your preference next time.</p>
    <p>
      To bottom-post, you have to highlight the portion of message that you
      want to quote.
    </p>
    <h3 id="gmail-web">Gmail</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>Compose a new message</li>
      <li>On the bottom right, click the three-dotted icon</li>
      <li>Select the plain text option</li>
    </ol>
    <p>Gmail will remember your preference next time.</p>
    <p>Gmail wraps plain text messages at 78 characters.</p>
    <p>
      While composing a reply to a post, click the three horizontal dots at
      the bottom of the editor. This will reveal the quote of the previous
      post. Your response can be typed below this quote to bottom post.
    </p>
    <h3 id="gyazmail">Gyazmail</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <p>Gyazmail has the correct settings by default.</p>
    <h3 id="k-9">K-9 Mail</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>Tap the three lines on the top left</li>
      <li>Select Settings</li>
      <li>Tap the email address you wish to configure → Sending mail</li>
      <li>Change the Message Format to Plain Text</li>
      <li>Select "Reply after quoted text"</li>
    </ol>
    <p>
      It is also recommended that you select "Manage identities", and for each
      identity, untick "Use Signature". This will remove "Sent from K-9" from
      your emails.
    </p>
    <h3 id="mailbox-org">mailbox.org</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <ol>
      <li>Click the "gear" icon on the top right</li>
      <li>Go to the E-mail section</li>
      <li>Uncheck "Allow HTML formatted email messages" under "View"</li>
      <li>Go to the Compose sub-section</li>
      <li>Select "Plain text" under "Format email messages as"</li>
    </ol>
    <p>
      You can request better plain text support via their
      <a rel="noopener nofollow" href="https://userforum-en.mailbox.org">
        user forum</a>.
    </p>
    <h3 id="mailspring">Mailspring</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <ol>
      <li>Tap the three lines on the top right</li>
      <li>Select Preferences → General</li>
      <li>Unselect "Enable rich text and advanced editor features"</li>
    </ol>
    <p>
      Mailspring supports basic plain text composing. You can also create a
      single plain-text email by holding Alt or Option while clicking
      "Compose" or "Reply". Request more features by
      <a
        rel="noopener nofollow"
        href="https://github.com/Foundry376/Mailspring/issues/52"
      >asking them for it</a>.
    </p>
    <h3 id="outlook-desktop">Microsoft Outlook (Desktop)</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <ol>
      <li>Select File → Options → Mail</li>
      <li>
        Under "Compose Messages", select "Plain Text" from the "Compose messages
        in this format" options.
      </li>
    </ol>
    <h3 id="outlook-web">Microsoft Outlook (Web)</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <ol>
      <li>Click the gear icon on the top right</li>
      <li>Click "View all Outlook settings"</li>
      <li>Go to Mail → Compose and reply</li>
      <li>Scroll down to "Message format"</li>
      <li>Change HTML to Plain text</li>
      <li>Click "Save"</li>
    </ol>
    <h3 id="nine">Nine</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <ol>
      <li>Open the side menu and click the "gear" icon</li>
      <li>Select General, then Composer</li>
      <li>Under Editor, change the Editor option to "Text editor"</li>
    </ol>
    <h3 id="posteo">Posteo</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <p>
      <strong>Notice</strong>: Users are susceptible to vendor lock-in as Posteo does
      not support custom domain names.
    </p>
    <ol>
      <li>Open Settings by clicking the menu item on the top right</li>
      <li>Select Preferences in the top left</li>
      <li>Go to Composing Messages in the navigation tree on the left</li>
      <li>Select "never" from the drop-down menu next to the "Compose HTML messages" line</li>
      <li>Select "start new message below original" from the drop-down menu next to the "When replying" line</li>
    </ol>
    <h3 id="protonmail">Proton Mail</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <p>
      <strong>Notice</strong>: Use of IMAP and SMTP with Proton Mail requires
      the use of a bridge. A
      <a
        href="https://github.com/emersion/hydroxide"
        rel="nofollow noopener"
      >third-party, MIT-licensed bridge</a> is available.

      Proton technologies also makes
      <a
        href="https://github.com/ProtonMail/proton-bridge"
        rel="nofollow noopener"
      >the source code to their official bridge</a> available under the GNU-GPL-3.0-or-later.
    </p>
    <ol>
      <li>Visit Settings → Appearance</li>
      <li>Set "Composer Mode" to "Plain Text"</li>
    </ol>
    <p>
      It's also recommended that you visit Settings → Account → Identity and remove
      the default email signature.
    </p>
    <h3 id="rainloop">Rainloop</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <ol>
      <li>Click the "person" icon on the top-right corner and choose "Settings"</li>
      <li>Set "Default text editor" to "Plain"</li>
    </ol>
    <h3 id="roundcube">Roundcube</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <p>
      <strong>Notice</strong>: Depending on how your system administrator
      configured roundcube, or how old the downloaded roundcube is,
      you might have to do the following steps, but on most cases you shouldn't
      have to.
    </p>
    <ol>
      <li>Click Settings on the top-right</li>
      <li>Select Preferences → Composing Messages</li>
      <li>Select "never" under "Compose HTML messages"</li>
      <li>Select Composing Messages</li>
      <li>Select "Start new message below the quote" under "When replying"</li>
    </ol>
    <p>Also consider setting "Automatically add signature" to "never".</p>
    <h3 id="runbox7">Runbox 7</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <p>Runbox 7 uses plain text email by default.</p>
    <h3 id="smail">Smail</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <p>Smail uses plain text email by default.</p>
    <h3 id="sogo">Sogo</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>On the top-right, click the gear to open Preferences</li>
      <li>Select the Mail tab</li>
      <li>Change "Compose messages in" to "Plain text"</li>
      <li>Click the green "Save" icon on the top-right</li>
    </ol>
    <h3 id="sylpheed">Sylpheed</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>Open Configuration → Common preferences</li>
      <li>Visit the Compose → Editor tab</li>
      <li>Enable "Wrap on input"</li>
    </ol>
    <h3 id="thunderbird">Thunderbird</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="green">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>In the accounts tab, right click on your account</li>
      <li>Select "Settings"</li>
      <li>Select the "Composition &amp; Addressing" settings</li>
      <li>Disable "Compose messages in HTML format"</li>
      <li>Select "Start my reply below the quote"</li>
      <li>Repeat this for any other email accounts</li>
    </ol>
    <h3 id="thunderbird-android">Thunderbird for Android</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="green">Supports bottom posting</span>
    </div>
    <ol>
      <li>Tap the three lines on the top left</li>
      <li>Tap the wheel to enter Settings</li>
      <li>Tap the email address you wish to configure → Sending mail</li>
      <li>Change the Message Format to Plain Text</li>
      <li>Select "Reply after quoted text"</li>
    </ol>
    <p>
      It is also recommended that you select "Manage identities", and for each
      identity, untick "Use Signature". This will remove "Sent from K-9" from
      your emails.
    </p>
    <h3 id="tutanota">Tuta</h3>
    <div class="breakdown">
      <span class="green">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <p>
      <strong>Notice</strong>: Use of IMAP and SMTP, open standards for email
      clients, is not possible with Tuta. This is not acceptable behavior
      for an email provider and use of Tuta is strongly recommended against
      for this reason. Tuta's stated reasons for not supporting these
      protocols are lies and you would be well served by closing your account
      there.
    </p>
    <ol>
      <li>Visit Settings → Email → Formatting</li>
      <li>Set the format to plain text</li>
    </ol>
    <h3 id="windows-mail">Windows Mail</h3>
    <div class="breakdown">
      <span class="red">Compose in plain text</span>
      <span class="red">Wraps text or format=flowed</span>
      <span class="red">Supports bottom posting</span>
    </div>
    <p>
      Windows Mail does not support plain text email. Please issue a feature
      request through the Feedback Hub app, and use a different mail client.
    </p>
    <h2 id="etiquette"><a href="#etiquette">
      Etiquette recommendations for plain text emails
    </a></h2>
    <p>
      A few notes on the subject of plain text email etiquette:
    </p>
    <h3>Top posting</h3>
    <p>
      When you reply to an email, many email clients will include a quoted
      version of the entire message that you are responding to beneath your
      reply. This leads to long email threads which contain the entire history
      of the discussion in an increasingly long trailer on every email. This is
      called "top posting", and it's strongly frowned upon by many users of
      plain text email.
    </p>
    <p>
      Plain text email makes it easier to incorporate the original text into
      your reply much more meaningfully. Consider these two examples:
    </p>
<pre>Yes, that sounds good.

Jim said on 2019-03-23 at 11:02 PM:
> Are you okay with maroon?
>
> Tim said on 2019-03-23 at 10:43 PM:
>> Do we know what color we should use for the background?
>> 
>>> Jim said on 2019-03-23 at 10:30 PM:
>>> Is there anything left to do on the site?</pre>
    <p>
      This email uses top-posting, and this approach is discouraged. A better
      approach might look more like this:
    </p>
<pre>Jim said on 2019-03-23 at 11:02 PM:
> Are you okay with maroon?

Yes, that sounds good.</pre>
    <p>
      You can also edit the original email more, quoting it several times to
      make it clear what points you are responding to, like so:
    </p>
<pre>Hey Drew,

Can you look into the bug which is causing 2.34 clients to disconnect
immediately? I think this is related to the timeouts change last week.

Also, your fix to the queueing bug is confirmed for the next release,
thanks!</pre>
<pre>Hey Sarah, I can look into that for sure.

> I think this is related to the timeouts change last week.

I'm not so sure. I think reducing the timeouts would *improve* this issue,
if anything. I'll look into it.

> Also, your fix to the queueing bug is confirmed for the next release,
> thanks!

Sweet! Happy to help.</pre>
    <p>As the old joke goes...</p>
    <ul style="list-style: none; padding: 0;">
      <li><strong>A</strong>: Because it reverses the logical flow of conversation.</li>
      <li><strong>Q</strong>: Why is top posting frowned upon?</li>
    </ul>
    <h3>Wrapping your text</h3>
    <p>
      Plain text emails are generally encouraged to be wrapped at 72 columns,
      by inserting a newline and resuming the content on the next line. This is
      encouraged to make email more comfortable to read and quote in many of
      the contexts where technical users may encounter it, such as terminal
      emulators. Of course, it's far too annoying to do this manually as you
      write &mdash; the <a href="#recommended-clients">recommended clients</a>
      will do this for you, as well as any client shown above with "Wraps text
      or uses format=flowed".
    </p>
    <h3>Further recommendations</h3>
    <p>
      <a href="https://www.ietf.org/rfc/rfc1855.txt"
         rel="noopener nofollow">This document</a>
      can serve as an additional guide to email communication etiquette.
    </p>
    <h2 id="why-plaintext"><a href="#why-plaintext">
      Why is plain text better than HTML?
    </a></h2>
    <p>
      HTML emails are mainly used for marketing - that is, emails you probably
      don't want to see in the first place. The few advantages they offer for
      end-users, such as links, inline images, and bold or italic text, aren't
      worth the trade-off.
    </p>
    <h3>HTML as a vector for phishing</h3>
    <p>
      HTML emails allow you to make links which hide the URL behind some
      user-friendly text. However, this is an extremely common vector for
      phishing attacks, where a malicious sender makes a misleading link which
      takes you to a different website than you expect. Often these websites are
      modeled after the login page of a service you use, and will trick you into
      entering your account password. In plain text emails, the URL is always
      visible, and you can more easily make an informed choice to click it.
    </p>
    <p>
      Many phishing emails have also taken the step of carefully replicating
      the visual style of an email you might trust, such as the appearance of a
      PayPal email. With plain text, it's much more difficult to trick you like
      this.
    </p>
    <h3>Privacy invasion and tracking</h3>
    <p>
      Virtually all HTML emails sent by marketers include identifiers in links
      and inline images which are designed to extract information about you and
      send it back to the sender. Examine the URLs closely - the strange numbers
      and letters are unique to you and used to identify you. This information
      is used to hack your brain, attempting to find advertisements which are
      more likely to influence your buying habits. HTML emails are good for
      marketers and bad for you.
    </p>
    <h3>Higher incidence of spam</h3>
    <p>
      HTML emails open up a lot of possibilities which are exploited by
      spammers to circumvent spam filters, such as making large amounts of text
      invisible, using hidden elements, and so on. Many people discard HTML
      emails (particularly mailing lists) on the simple basis that it
      dramatically reduces the amount of spam emails they receive.
    </p>
    <h3>Mail client vulnerabilities</h3>
    <p>
      HTML is an extremely large and complicated set of specifications designed
      without emails in mind. It's designed for browsing the world wide web, on
      which a huge variety of documents, applications, and more are available.
      Implementing even a reasonable subset of these standards represents
      hundreds of thousands of hours of work, or even millions. A large subset
      (perhaps the majority) of these features are not desirable for emails, and
      if included can be leveraged to leak information about you, your contacts,
      your calendar, other emails in your inbox, and so on. However, because of
      the herculean effort necessary to implement an HTML renderer, no one has
      built one specialized for emails which is guaranteed to be safe. Instead,
      general purpose web browsers, with many of their features disabled, are
      employed in most email clients. This is the number one source of
      vulnerabilities in email clients which result in information disclosure
      and even the execution of arbitrary malicious code.
    </p>
    <p>
      <a
        href="https://www.cvedetails.com/vulnerability-list.php?vendor_id=452&amp;product_id=3678&amp;version_id=&amp;page=1&amp;hasexp=0&amp;opdos=0&amp;opec=1&amp;opov=0&amp;opcsrf=0&amp;opgpriv=0&amp;opsqli=0&amp;opxss=0&amp;opdirt=0&amp;opmemc=0&amp;ophttprs=0&amp;opbyp=0&amp;opfileinc=0&amp;opginf=0&amp;cvssscoremin=0&amp;cvssscoremax=0&amp;year=0&amp;month=0&amp;cweid=0&amp;order=3&amp;trc=421&amp;sha=e340a73ad8b935c928250f8f7668f55ce061c8ec"
        rel="noopener nofollow"
      >This is a list</a> of 421 remote code execution vulnerabilities in
      Thunderbird. If you're bored, try finding one that doesn't exploit web
      tech.
    </p>
    <h3>HTML emails are less accessible</h3>
    <p>
      Browsing the web is a big challenge for users who require a screenreader
      or other assistive tools to use their computer. The same problems apply to
      email, only more so - making an accessible HTML email is even more
      difficult than making an accessible website due to the limitations imposed
      on HTML emails by most mail clients (which they have no choice but to
      impose - for the security reasons stated above). Plain text emails are
      a breeze in comparison for screenreaders to recite, especially for users
      with specialized email clients designed for this purpose. How do you speak
      bold text aloud? How about your inline image?
    </p>
    <h3>Some clients can't display HTML emails at all</h3>
    <p>
      Some email clients don't support HTML emails at all. Many email clients
      are designed to run in text-only environments, like a terminal emulator,
      where they're useful to people who spend a lot of time working in these
      environments. In a text-only interface it's not possible to render an HTML
      email, and instead the reader will just see a mess of raw HTML text. A lot
      of people simply send HTML emails directly to spam for this reason.
    </p>
    <h3>Rich text isn't that great, anyway</h3>
    <p>
      Rich text features desirable for end users include things like inline
      images, bold or italicized text, and so on. However, the tradeoff isn't
      worth it. Images can simply be attached to your email, and you can employ
      things like *asterisks*, /slashes/, _underscores_, or UPPERCASE for
      emphasis. You can still communicate your point effectively without
      bringing along all of the bad things HTML emails come with.
    </p>
    <h2 id="implementation-recommendations"><a href="#implementation-recommendations">
      Recommendations for software which sends emails
    </a></h2>
    <p>
      Thanks for your interest in making plain text emails more accessible to
      your users!
    </p>
    <h3>For senders of automated emails</h3>
    <ul>
      <li>
        If you must use HTML, send a
        <a
          href="https://en.wikipedia.org/wiki/MIME"
          rel="noopener nofollow"
        >multipart/alternative</a>
        email with both a plain text and HTML version included. Your programming
        language probably includes tools for creating these kinds of emails.
      </li>
    </ul>
    <h3>For senders of user-authored emails</h3>
    <ul>
      <li>
        Allow users to compose emails in plain text, and consider making this
        the default. When the user composes a message in plain text, send a
        text/plain email instead of a multipart/alternative email with a
        generated HTML part.
      </li>
      <li>
        When composing a plain text email, hard-wrap their lines at 72 columns by
        inserting newlines at the nearest word boundary. Optionally, use
        <a
          href="https://www.ietf.org/rfc/rfc3676.txt"
          rel="noopener nofollow"
        >format=flowed</a> instead to
        allow plain text emails to be displayed and composed without hard
        wrapping.
      </li>
      <li>
        When users reply to an email in plain text, don't encourage top-posting
        by leaving their cursor above the quoted message. Instead, add two
        newlines after the quoted message and leave their cursor there. Allow
        the user to edit the quoted message. Quote each line of the original
        message with &gt; and a single space, followed by the quote.
      </li>
      <li>
        When users send an HTML email explicitly, convert it to text/plain and
        send a multipart/alternative email which includes both versions.
      </li>
      <li>
        When displaying multipart/alternative emails, give the user the option to prefer
        to view the plain text part by default.
      </li>
    </ul>
    <p>
      Email clients which meet all of these criteria and prefer plain text by
      default are entitled to a spot on our recommended clients list.
    </p>
    <h2 id="contribute"><a href="#contribute">
      How to improve these recommendations
    </a></h2>
    <p>
      Do you want to add instructions for a new email client? Have suggestions
      or questions? Please send a <strong>plain text</strong> email to
      <a
        href="mailto:~sircmpwn/public-inbox@lists.sr.ht"
      >~sircmpwn/public-inbox@lists.sr.ht</a>. You can also
      <a href="https://git-send-email.io">email a patch</a> to this address. The
      code may be found on
      <a href="https://git.sr.ht/~sircmpwn/useplaintext.email">git.sr.ht</a>.
    </p>
    <hr />
    <p class="muted">
      "But if plain text is so good, why is this page written in HTML?"
      <br />
      This is a reference document, not an email!
    </p>
    <p class="muted">
      This site is distributed with the <a
        href="https://git.sr.ht/~sircmpwn/useplaintext.email/tree/master/LICENSE"
      >MIT license</a>. "Plain Text Certified" graphic by
      <a href="https://fosstodon.org/@ohyran">Jens</a>, <a
        href="https://creativecommons.org/licenses/by-sa/4.0/"
      >CC-BY-SA</a>.
    </p>
  </body>
</html>
